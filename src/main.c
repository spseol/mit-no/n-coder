#include "main.h"
#include "milis.h"
#include "uart1.h"
#include <stdbool.h>
#include <stdio.h>
#include <stm8s.h>

#define NCCLK_PORT GPIOB
#define NCCLK_PIN  GPIO_PIN_2
#define NCDATA_PORT GPIOB
#define NCDATA_PIN  GPIO_PIN_3

void init(void) {
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // taktovani MCU na 16MHz
    GPIO_Init(NCCLK_PORT, NCCLK_PIN, GPIO_MODE_IN_FL_NO_IT);
    GPIO_Init(NCDATA_PORT, NCDATA_PIN, GPIO_MODE_IN_FL_NO_IT);

    init_milis();
    init_uart1();
}



int8_t last = 0;
int check_ncoder(void)
{
    int8_t r, now;

    now = (bool)PUSH(NCCLK);

    if (last && !now) {
        if ((bool)PUSH(NCDATA))
            r = 1;
        else
            r = -1;
    }
    if (!last && now) {
        if ((bool)PUSH(NCDATA))
            r = -1;
        else
            r = 1;
    }

    last = now;
    return r;
}

int main(void) {

    uint32_t time1 = 0;
    uint32_t time2 = 0;
    int16_t count = 0;

    init();
    printf("START\n");

    while (1) {
        if (milis() - time1 > 3) {
            time1 = milis();
            count += check_ncoder();
        }
        if (milis() - time2 > 333) {
            time2 = milis();
            printf("%d\n", count);
        }
    }
}

/*-------------------------------  Assert -----------------------------------*/
#include "__assert__.h"
